<?php

require "DateRangeHelper.php";

class DateRangeHelperTest extends PHPUnit_Framework_TestCase {
    
    public function setUp() { }
    public function tearDown() { }
    
    /**
    * @param string $date11
    * @param string $date12
    * @param string $date21
    * @param string $date22
    * @param  $interval2
    * @param  $repetitions2
    * @return boolean $result
    *
    * @dataProvider providerTestDoesOverlap
    */
    public function testDoesOverlap($date11, $date12, $date21, $date22, $interval2, $repetitions2, $result) {
                
        $dtUser = new DateRangeHelper($date11, $date12);
        $dtEvent = new DateRangeHelper($date21, $date22, $interval2, $repetitions2);
        
        // One way test
        $testResult = $dtUser->doesOverlap($dtEvent);
        $this->assertEquals($result, $testResult);
        
        // Reversed test
        $reversedTestResult = $dtEvent->doesOverlap($dtUser);
        $this->assertEquals($result, $reversedTestResult);
    }
    
    public function providerTestDoesOverlap() {
        return [
            ['2016-01-05', '2016-02-10', '2016-02-13', '2016-03-31', null, null, false],
            ['2016-01-05', '2016-02-10', '2016-02-03', '2016-03-31', null, null, true],
            ['2016-01-05', '2016-02-10', '2015-09-13', '2016-01-06', null, null, true],
            ['2016-01-05', '2016-02-10', '2016-01-04', '2016-01-04', null, null, false],
            [null, '2016-02-10', '2016-01-04', '2016-04-04', null, null, true],
            [null, '2016-02-10', '2015-01-04', '2015-04-04', null, null, true],
            [null, '2016-02-10', '2016-03-04', '2016-04-04', null, null, false],
            ['2016-05-12', null, '2016-03-04', '2016-04-04', null, null, false],
            ['2016-05-12', null, '2016-06-04', '2016-09-04', null, null, true],
            ['2016-05-12', null, '2016-03-04', '2016-08-04', null, null, true],
            
            // Intervals
            ['2016-01-05', '2016-02-10', '2016-02-13', null, 1, 0, false],
            ['2016-01-05', '2016-02-10', '2016-01-13', null, 1, 0, true],
            ['2016-01-05', '2016-02-10', '2016-01-13', null, 4, 0, true],
            ['2016-01-05', '2016-02-10', '2016-01-01', null, 4, 4, false],
            
            ['2016-04-05', '2016-04-10', '2016-01-01', null, 3, 12, false],
            ['2016-04-05', '2016-05-10', '2016-01-01', null, 3, 12, true],
            ['2016-04-01', '2016-04-01', '2016-01-01', null, 3, 12, true],
            ['2015-04-01', '2016-01-01', '2016-01-01', null, 4, 12, true],
            [null, '2016-01-01', '2016-01-01', null, 4, 12, true],
            [null, '2016-12-31', '2017-01-01', null, 1, 10, false],
            ['2011-05-16', null, '2016-01-01', null, 1, 2, true],
            ['2017-03-05', '2017-03-31', '2017-02-20', null, 3, 1, false],
            ['2017-03-05', '2017-03-31', '2017-02-20', null, 3, 2, true],
        ];
    }
}