<?php

/**
 * DateRangeHelper checks if two date ranges overlap. 
 * One of the ranges can be a range with interval - an 'event' 
 * occuring between start and end of the range.
 *
 * @author Alan
 */
class DateRangeHelper {
    /** @var DateTime $from */
    private $from;
    /** @var DateTime $to */
    private $to = null;
    
    /** @var int $interval                      1:day || 2:week || 3:month || 4:year */
    private $interval = -1;
    /** @var int $repetitions                   n:limited || 0:unlimited */
    private $repetitions = -1;
    /** @var bool $isIntervalEvent */
    private $isIntervalEvent = false;
    /** @var DateTime $previousRecurringDate    Date of the previous recurring event. */
    private $previousRecurringDate = null;
    /** @var DateTime $nextRecurringDate        Date of the next recurring event. */
    private $nextRecurringDate = null;
    /** @var string $dateFormat                 Expected datetime format */
    private $dateFormat = 'Y-m-d H:i:s';
    
    /**
     * DateTime $to AND ($interval AND $repetitions) are mutually exclusive. 
     * In case bot are present DateTime $to takes precedence.
     * @param string $from
     * @param string $to
     * @param int $interval
     * @param int $repetitions
     */
    public function __construct($from, $to, $interval = null, $repetitions = null) {
        // Assign start date
        if($this->isDateCorrect($from, null, true)) { 
            $this->from = new DateTime();
            $this->from->setTimestamp(strtotime($from));
        } else { 
            $this->from = DateTime::createFromFormat($this->dateFormat, '0001-01-01 00:00:00'); 
        }
        
        
        if($this->isDateCorrect($to, null, true, '23:59:59')) { 
            // Date $to is present and correct - this takes precedence even if interval is not missing
            $this->to = new DateTime();
            $this->to->setTimestamp(strtotime($to));
            if($this->from > $this->to) { throw new \Exception("'From' date should be earlier or equal to 'to' date."); }
        } else if ($interval === null && $repetitions === null) { 
            // Date $to is missing, interval is missing - assigning 'infinite' date
            $this->to = DateTime::createFromFormat($this->dateFormat, '2100-12-31 00:00:00'); 
        } else if ($interval !== null && $repetitions !== null && $interval >= 1 && $interval <= 4 && $repetitions >= 0) {
            // Interval is not missing
            $this->isIntervalEvent = true;
            $this->interval = (int) $interval;
            $this->repetitions = (int) $repetitions;
            $this->setIntervalEndDate();
            $this->getRecurringEventDates();
        } else {
            throw new \Exception("Provided interval data is not correct.");
        }
    }    
    
    /**
     * Checks if the date is correct.
     * @param string    $stringDate     Date in given format. Month and day must be padded with 0s.
     * @param string    $format
     * @param boolean   $dateWithTime   Indicates whether the date is with time or not.
     * @param strint    $defaultTime    If the date comes with time but string is without then add this default time to it.
     * @return boolean
     */
    private function isDateCorrect($stringDate = false, $format = null, $dateWithTime = true, $defaultTime = '00:00:00') {
        if(!$stringDate) { return false; }      
      
        if(!$format) { $format = $this->dateFormat; }
        if(!strtotime($stringDate)) { return false; }
        if($dateWithTime) {
            $dateArr = date_parse($stringDate);
            // Add time to date if parsed time is false = time is missing.
            if($dateArr['minute'] === false) {
                $stringDate = $stringDate.' '.$defaultTime;
            }
        }  
        $date = DateTime::createFromFormat($format, $stringDate);
        return $date && $date->format($format) == $stringDate;
    }
    
    /**
     * Sets period end date for finite intervals.
     */
    private function setIntervalEndDate() {
        if($this->repetitions === 0) { 
            // The event runs indefinitely
            $this->to = DateTime::createFromFormat($this->dateFormat, '2100-12-31 23:59:59');  
        } else {
            $sumPeriodIntervals = $this->getDateInterval($this->interval, $this->repetitions);
            $this->to = clone $this->from;
            $this->to->add($sumPeriodIntervals);
        }
    }
    
    /**
    * Checks if the current object overlaps with the 
    * DateRangeHelper object passed in $other.
    * @param DateRangeHelper $other
    * @return boolean
    */
    public function doesOverlap(DateRangeHelper $other) {
        if($this->isIntervalEvent && $other->isIntervalEvent) { throw new \Exception("Cannot compare two interval events."); }
        if(!$this->isIntervalEvent && !$other->isIntervalEvent) {
            // Both dates are defined periods
            return !(($this->to < $other->from) || ($this->from > $other->to));
        }
      
        if($this->isIntervalEvent) {
            $interval = $this;
            $period = $other;
        } else {
            $interval = $other;
            $period = $this;
        }

        // Don't overlap because - period outside interval period
        if($interval->from > $period->to || $interval->to < $period->from) { return false; }
    
        // Period is within interval period. Check if it overlaps with any of interval dates
        $currentIntervalDate = clone $interval->from;
        $dateInterval = $this->getDateInterval($interval->interval);
        $i = 1;
        while(true) {
            // Reached the last repetition and haven't found overlapping dates.
            if($interval->repetitions !== 0 && $i > $interval->repetitions) { return false; }
            // Check if current interval date and the period overlap.
            if($this->periodContainsDate($period, $currentIntervalDate)) { return true; }           
            $currentIntervalDate = $currentIntervalDate->add($dateInterval);
            // Interval event date went beyond period 'to' date.
            if($currentIntervalDate > $period->to) { return false; }
            else { $i++; }
        }
    }
    
    /**
     * Checks if container of size 1-n days contains a date(day).
     * @param DateRangeHelper   $container  Can consist of 1 - n days.
     * @param DateTime          $date       This is a single date(day).
     * @return bool
     */
    private function periodContainsDate(DateRangeHelper $container, DateTime $date) {
        if($container->from <= $date && $container->to >= $date) { return true; }
        else { return false; }
    }
    
    /**
     * Creates a DateInterval object.
     * @param int $unit     unit of time to be added 1:day || 2:week || 3:month || 4:year
     * @return DateInterval
     */
    private function getDateInterval($unit, $repetitions = 1) {
        $repetitions = $repetitions < 0 ? 1 : $repetitions;
        $dIntStr = 'P'.$repetitions;
            switch($unit) {
            case '1':
                $dIntStr .= 'D';
                break;
            case '2':
                $dIntStr .= 'W';
                break;
            case '3':
                $dIntStr .= 'M';
                break;
            case '4':
                $dIntStr .= 'Y';
                break;
            default:
                $dIntStr .= 'D';
        };
        return new DateInterval($dIntStr);
    }
    
    /**
     * Gets next and previous dates of recurring event relative to now.
     * @return void
     */
    private function getRecurringEventDates() {
        if(!$this->isIntervalEvent) { return; }
        $now = new DateTime();
        // The event has end date and it has finished
        if($this->to && $now >= $this->to) { 
            $this->previousRecurringDate = clone $this->to;
            return; 
        }

        // The event hasn't started yet
        if($now < $this->from) {

            $this->previousRecurringDate = null;
            $this->nextRecurringDate = clone $this->from;
            return;
        }
        $interval = $this->getDateInterval($this->interval);

        $nextDate = clone $this->from;
        $previousDate = null;
        while($nextDate <= $now) {
            $previousDate = clone $nextDate;
            // This shouldn't happen.
            if($nextDate >= $this->to) { return; }
            $nextDate->add($interval);
        }
        $this->previousRecurringDate = $previousDate;
        $this->nextRecurringDate = $nextDate;
    }
    
    /**
     * Property getter.
     * @param String $name
     * @return type
     */
    public function __get($name) {
        return isset($this->{$name}) ? $this->{$name} : false;
    }
}