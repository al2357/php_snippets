Collection of PHP snippets:

* PaginationHelper - pagination class compatible with Twitter Bootstrap (tested on v3.3.5).
* DateRangeHelper -  checks if two date ranges overlap. One of the ranges can be a range with interval - an 'event' occuring between start and end of the range.