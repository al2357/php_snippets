<?php
/**
 *      | << | < | 1 | 2 | 3 | 4 | 5 | > | >> |
 * Pagination helper. Compatible with Twitter Bootstrap.
 * Tested on v3.3.5.
 * 
 * @author Alan Skibinski <alan.skibinski[at]gmail.com>
 */
class PaginationHelper {
    /* @var int $page               Current page number. */
    protected $page;
    
    /* @var int $pageSize           Number of records on a page. */
    protected $pageSize = 20;
    
    /* @var string $baseUrl         Base URL of a page, without GET parameters. */
    protected $baseUrl = '';
    
    /* @var string $getParameters   GET parameters without p. */
    protected $getParameters = '';
    
    /* @var int $totalRecords       Total number of records. */
    protected $totalRecords;
    
    /* @var int $padding            Pagination padding - number of pages on each side of an active page. */
    protected $padding = 2;
    
    /* @var array $skipParameters   Skip GET parameters. */
    private $skipParameters = ['p'];

    /**
    *   Class constructor
    *   @param int      $totalRecords   Total number of records to be displayed.
    *   @param int      $pageSize       Max. number of records per page.
    *   @param string   $baseUrl        Base url of a page, without GET parameters.
    *   @param array    $skipParameters URL parameters to skip - don't copy if present. 
    */
    function __construct($totalRecords, $pageSize = null, $baseUrl = null, $skipParameters = []) {
        $p = (int) Request::get('p');
        $this->page = $p ? $p : 1;
        $this->totalRecords = (int) $totalRecords;
        $this->skipParameters = array_merge($this->skipParameters, $skipParameters); 
        if($pageSize) { $this->pageSize = $pageSize; }
        
        if(!$baseUrl) {
            $redirectUrl = $_SERVER['REDIRECT_URL'];
            $redirectUrl = strpos($redirectUrl, '/') == 0 ? substr($redirectUrl, 1) : $redirectUrl;
            $baseUrl = Config::get('URL').$redirectUrl; 
        }
        $this->baseUrl = $baseUrl;
       
        $requestUriArr = parse_url($_SERVER['REQUEST_URI']);
        if(isset($requestUriArr['query'])) {
            $queryArr = []; 
            parse_str($requestUriArr['query'], $queryArr);
            foreach($queryArr as $qKey=>$qVal) {
                if(in_array($qKey, $this->skipParameters)) { continue; }
                $this->getParameters .= '&'.$qKey.'='.$qVal;
            } 
        }
    }
    
    /**
    *   Gets pagination html.
    *   @return string
    */
    public function getHtml() {
        if($this->totalRecords <= $this->pageSize) { return '<div class="pagination-wrap" style="height:119px"></div>'; }
        $pagesQty = ceil($this->totalRecords / $this->pageSize);

        if ($pagesQty <= 5 || ($this->page - $this->padding <= 0)) { $startAt = 1; }
        else if ($this->page >= ($pagesQty - $this->padding)) { $startAt = $pagesQty - (2 * $this->padding); }
        else { $startAt = $this->page - 2; }
        $length = $pagesQty <= (2 * $this->padding + 1) ? $pagesQty : (2 * $this->padding + 1);
        $endAt = $startAt + $length;
        $buttons = [];
        $prevArrowClass = ($this->page == 1) ? ' class="disabled"' : '';
        $prevPageId = ($this->page == 1) ? 1 : ($this->page - 1);
        $buttons[] = '<nav aria-label="Page navigation" class="pagination">';
        $buttons[] = '<ul class="pagination">';
        $buttons[] = '<li'.$prevArrowClass.'><a aria-label="First" href="'.$this->baseUrl.'?p=1'.$this->getParameters.'">'
                . '<span aria-hidden="true">&laquo;</span>'
                . '</a></li>';
        $buttons[] = '<li'.$prevArrowClass.'><a aria-label="Previous" href="'.$this->baseUrl.'?p='.$prevPageId.$this->getParameters.'">'
                . '<span aria-hidden="true">&lsaquo;</span>'
                . '</a></li>';
        
        for($i = $startAt; $i < $endAt; $i++) {
            $liClass = $i == $this->page ? ' class="active"' : '';
            $buttons[] = '<li'.$liClass.'><a href="'.$this->baseUrl.'?p='.$i.$this->getParameters.'">'.$i.'</a></li>';
        }
        $nextArrowClass = ($this->page == $pagesQty) ? ' class="disabled"' : '';
        $nextPageId = ($this->page == $pagesQty) ? $pagesQty : ($this->page + 1);
        $buttons[] = '<li'.$nextArrowClass.'><a aria-label="Next" href="'.$this->baseUrl.'?p='.$nextPageId.$this->getParameters.'">'
                . '<span aria-hidden="true">&rsaquo;</span>'
                . '</a></li>';
        $buttons[] = '<li'.$nextArrowClass.'><a aria-label="Last" href="'.$this->baseUrl.'?p='.$pagesQty.$this->getParameters.'">'
                . '<span aria-hidden="true">&raquo;</span>'
                . '</a></li>';
        $buttons[] = '</ul></nav>';
        return implode(PHP_EOL, $buttons);
    }
}
